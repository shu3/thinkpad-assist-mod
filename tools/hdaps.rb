def hdaps
  hdaps_cal_file = "/sys/devices/platform/hdaps/calibrate"
  hdaps_pos_file = "/sys/devices/platform/hdaps/position"
  cal_x, cal_y = File.read(hdaps_cal_file).gsub(/[\(\)]/, "").split(",").map(&:to_i)
  pos_x, pos_y = File.read(hdaps_pos_file).gsub(/[\(\)]/, "").split(",").map(&:to_i)
  puts "X=#{(pos_x - cal_x)*5}, Y=#{(pos_y - cal_y)*5}"
end

loop do
  hdaps
  sleep 0.1
end
