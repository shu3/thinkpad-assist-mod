@sample = []
@sample_length = 10
@skip_sample = 0

def sample(xy)
  if @skip_sample > 0
    @skip_sample-=1
    return
  end
  @sample << xy
  if @sample.size > @sample_length
    @sample.shift
  end

  sum_x = 0
  sum_y = 0
  @sample.each do |x, y|
    sum_x += x
    sum_y += y
  end

  avg_x = sum_x / @sample.size.to_f
  avg_y = sum_y / @sample.size.to_f

  diff_total_x = 0
  diff_total_y = 0
  total_x = 0
  total_y = 0
  @sample.each do |x, y|
    diff_total_x += (x - avg_x).abs;
    diff_total_y += (y - avg_y).abs;
    total_x += x;
    total_y += y;
  end

  case
  when diff_total_y > 800 && total_y < -10
    puts "onDig"
  when (diff_total_x > 400)
    # b = @sample.index{|x, _| x <= 10 && x >= -10 }
    # if b
      if @sample.detect{|x, _| x > 200 }
        puts "onRight"
        @sample.clear
        @skip_sample = 50
      end
      if @sample.detect{|x, _| x < -200 }
        puts "onLeft"
        @sample.clear
        @skip_sample = 50
      end
    #end
  when diff_total_y > 400
    # b = @sample.index{|_, y| y <= 10 && y >= -10 }
    # if b
      if @sample.detect{|_, y| y > 200 }
        puts "onUp"
        @sample.clear
        @skip_sample = 50
      end
    # end
  end
end

def hdaps
  hdaps_cal_file = "/sys/devices/platform/hdaps/calibrate"
  hdaps_pos_file = "/sys/devices/platform/hdaps/position"
  cal_x, cal_y = File.read(hdaps_cal_file).gsub(/[\(\)]/, "").split(",").map(&:to_i)
  pos_x, pos_y = File.read(hdaps_pos_file).gsub(/[\(\)]/, "").split(",").map(&:to_i)
  [(pos_x - cal_x)*5, (pos_y - cal_y)*5]
end

loop do
  p hdaps
  sample(hdaps)
  sleep 0.01
end
