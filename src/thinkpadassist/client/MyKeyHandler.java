package thinkpadassist.client;

import java.util.EnumSet;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.settings.KeyBinding;
import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.TickType;

public class MyKeyHandler extends KeyHandler {
	public static KeyBinding keyBindFixAngle = new KeyBinding("Fix Camera Angle", Keyboard.KEY_LCONTROL);
	
	public MyKeyHandler() {
		super(new KeyBinding[] { keyBindFixAngle }, new boolean[] { true });
	}
	
	@Override
	public String getLabel() {
		return "ThinkPadAssist.KeyHandler";
	}
	
	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.CLIENT);
	}
	
	@Override
	public void keyUp(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd) {
		if (kb.keyCode == keyBindFixAngle.keyCode) {
//			System.out.println("ThinkPadAssist: keyUp!");
			keyBindFixAngle.pressed = false;
		}
	}
	
	@Override
	public void keyDown(EnumSet<TickType> types, KeyBinding kb,
			boolean tickEnd, boolean isRepeat) {
		if (kb.keyCode == keyBindFixAngle.keyCode) {
//			System.out.println("ThinkPadAssist: keyDown!");
			keyBindFixAngle.pressed = true;
		}
	}
}
