package thinkpadassist.client;

import java.util.EnumSet;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.registry.LanguageRegistry;

import thinkpadassist.CommonProxy;
import thinkpadassist.ThinkPad;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.src.ModLoader;
import net.minecraftforge.client.MinecraftForgeClient;

public class ClientProxy extends CommonProxy {
       
	@Override
	public void registerRenderers() {
	}
	
	@Override
	public void registerKeybindings() {
		KeyBindingRegistry.registerKeyBinding(new MyKeyHandler());
	}
}