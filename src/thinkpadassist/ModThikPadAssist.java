package thinkpadassist;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid="ThinkPadAssist", name="ThinkPadAssist", version="0.0.1")
@NetworkMod(clientSideRequired=true, serverSideRequired=false, versionBounds="1.6.2")
public class ModThikPadAssist {
	@Instance("ThinkPadAssist")
	public static ModThikPadAssist instance;
    public static final String ModId = "Shigeru";

    @SidedProxy(clientSide="thinkpadassist.client.ClientProxy", serverSide="thinkpadassist.CommonProxy")
    public static CommonProxy proxy;
   
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	try {
            Configuration config = new Configuration(event.getSuggestedConfigurationFile());
            config.load();
            config.save();
    	} catch (ExceptionInInitializerError e) {
			System.out.println(e.getCause().getMessage());
			e.getCause().printStackTrace();
			throw e;
		}
    }
   
    @EventHandler
    public void load(FMLInitializationEvent event) {
        proxy.registerRenderers();
    }
   
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	proxy.registerKeybindings();
    }
}
