package thinkpadassist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings;

import org.lwjgl.input.Mouse;

import thinkpadassist.client.ClientProxy;
import thinkpadassist.client.MyKeyHandler;

public class ThinkPad {
	static private final String CALIBLATE_FILE = "/sys/devices/platform/hdaps/calibrate";
	static private final String POSITION_FILE = "/sys/devices/platform/hdaps/position";
	
	public int x = 0;
	public int y = 0;
	public int absolute_x = 0;
	public int absolute_y = 0;
	public int dx = 0;
	public int dy = 0;
	
	private LinkedList<int[]> sampling_list;
	private static final int sampling_length = 10;
	private int skip_sampling = 0;
	
	public static boolean isSupportedPlatform() {
		return (new File(CALIBLATE_FILE)).exists();
	}

	public static ThinkPad instance = new ThinkPad();

	private ThinkPad() {
		super();
		sampling_list = new LinkedList<int[]>();
	}
	
	public void poll() {
//		System.out.println("ThinkPadAssist: X =" + this.x +
//				", Y =" + y + ", MouseX =" + Mouse.getX() + ", MouseY=" + Mouse.getY());
//		System.out.println("ThinkPadAssist: MouseDX =" + Mouse.getDX() +
//				", MouseDY=" + Mouse.getDY());
		try {
			String cal = (new BufferedReader(new FileReader(CALIBLATE_FILE))).readLine();
			String pos = (new BufferedReader(new FileReader(POSITION_FILE))).readLine();
			String[] cal_xy = cal.replace("(", "").replace(")", "").split(",");
			String[] pos_xy = pos.replace("(", "").replace(")", "").split(",");
			int poll_x = (Integer.valueOf(pos_xy[0]) - Integer.valueOf(cal_xy[0])) * 5;
			int poll_y = (Integer.valueOf(pos_xy[1]) - Integer.valueOf(cal_xy[1])) * 5;
			dx = poll_x - absolute_x;
			dy = poll_y - absolute_y;
			absolute_x = x = poll_x;
			absolute_y = y = poll_y;
			sample(x, y);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void sample(int x, int y) {
		if (skip_sampling > 0) {
			skip_sampling-=1;
			return;

		}

		int[] xy = {x, y};
		sampling_list.add(xy);
		if (sampling_list.size() > sampling_length) {
			sampling_list.removeFirst();
		}
		
		int sum_x = 0;
		int sum_y = 0;
		for (int[] _xy : sampling_list) {
			sum_x += _xy[0];
			sum_y += _xy[1];
		}
		
		float avg_x = sum_x / sampling_list.size();
		float avg_y = sum_y / sampling_list.size();
		
		int diff_total_x = 0;
		int diff_total_y = 0;
		int total_x = 0;
		int total_y = 0;
		for (int[] _xy : sampling_list) {
		    diff_total_x += Math.abs(_xy[0] - avg_x);
		    diff_total_y += Math.abs(_xy[1] - avg_y);
		    total_x += _xy[0];
		    total_y += _xy[1];
		}
		
		Minecraft.getMinecraft().gameSettings.keyBindJump.pressed = false;
		if (diff_total_y > 800 && total_y < -10) {
			System.out.println("ThinkPadAssist: onDig()");
			onDig();
		} else if (diff_total_x > 400) {
			int to_right = -1;
			int to_left = -1;
			for (int i = 0; i < xy.length; i++) {
				int[] _xy = sampling_list.get(i);
				if (_xy[0] > 200) {
					to_right = i;
					break;
				} else if (_xy[0] < -200) {
					to_left = i;
					break;
				}
			}
			if (0 <= to_right) {
				System.out.println("ThinkPadAssist: onRight()");
				onRight();
				sampling_list.clear();
				skip_sampling = 20;
			} else if (0 <= to_left) {
				System.out.println("ThinkPadAssist: onLeft()");
				onLeft();
				sampling_list.clear();
				skip_sampling = 20;
			}
		} else if (diff_total_y > 400) {
			int to_up = -1;
			int to_down = -1;
			for (int i = 0; i < xy.length; i++) {
				int[] _xy = sampling_list.get(i);
				if (_xy[1] > 200) {
					to_up = i;
					break;
				} else if (_xy[1] < -200) {
					to_down = i;
					break;
				}
			}
			if (to_up >= 0) {
				System.out.println("ThinkPadAssist: onUp()");
				onUp();
				sampling_list.clear();
				skip_sampling = 20;
			} else if (to_down >= 0) {
				System.out.println("ThinkPadAssist: onDown()");
			}
		}
	}
	
	public int getDX() {
		if (MyKeyHandler.keyBindFixAngle != null &&
				MyKeyHandler.keyBindFixAngle.pressed) {
			return 0;
		}
		int result = dx;
		dx = 0;
		return result;
	}
	
	public int getDY() {
		if (MyKeyHandler.keyBindFixAngle != null &&
				MyKeyHandler.keyBindFixAngle.pressed) {
			return 0;
		}
		int result = dy;
		dy = 0;
		return result;
	}
	
	public void onDig() {
		Minecraft.getMinecraft().gameSettings.keyBindAttack.pressTime++;
	}

	public void onUp() {
		Minecraft.getMinecraft().gameSettings.keyBindJump.pressed = true;
	}
	
	public void onLeft() {
		Minecraft.getMinecraft().thePlayer.inventory.changeCurrentItem(1);
	}
	
	public void onRight() {
		Minecraft.getMinecraft().thePlayer.inventory.changeCurrentItem(-1);
	}
}
